package com.frozenalex.xoxo.model

import android.util.Log

class Game (size:Int, winSize:Int) {
    var id: Long = 124
    var field: Array<Array<Int>>
    var size:Int = size
    var winSize = winSize
    var state: Int = 0
    var currentPlayer = 0

    init {
        field = Array(size, { _ -> Array(size, { _ -> -1}) }) // 2D array of ints
    }

    fun IsWon(x:Int, y: Int) : Boolean{
        var tempY = 0
        var tempX = 0
        var tempD = 0
        var tempO = 0

        val diff = x-y
        val sum = x+y

        for(i: Int in 0..size-1){
            // Vertical
            if(field[x][i] == currentPlayer) tempY++
            else tempY = 0

            // Horizontal
            if(field[i][y] == currentPlayer) tempX++
            else tempX = 0

            // Diagonals
            if (i+diff in 0..(size - 1)){
                if (field[i][i+diff] == currentPlayer) tempD++
                else tempD = 0
            }

            if (sum-i in 0..(size - 1)){
                if (field[i][sum-i] == currentPlayer) tempO++
                else tempO = 0
            }

            if (tempD >= winSize || tempO >= winSize || tempX >= winSize || tempY >= winSize) {
                Log.d("Game","Player $currentPlayer won!") // Log.d is faster
                return true
            }
        }
        return false
    }

    fun Move(x:Int,y:Int): Boolean{
        if (field[x][y] == -1) {
            field[x][y] = currentPlayer
            if(IsWon(x,y)){
                return true// Check if current player won
            }

            if (currentPlayer == 0)
                currentPlayer = 1
            else
                currentPlayer = 0
        }
        else Log.d("Game","Point $x  $y  occupied by player $field[x][y]")
        return false
    }


    companion object {
        // Game states
        val GAME_START = 0
        val GAME_GOING = 1
        val GAME_END = 2
    }
}
