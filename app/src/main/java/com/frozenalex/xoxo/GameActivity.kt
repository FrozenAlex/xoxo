package com.frozenalex.xoxo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import com.frozenalex.xoxo.model.Game
import com.frozenalex.xoxo.view.DrawingView
import com.frozenalex.xoxo.view.GLDrawingView

class GameActivity : AppCompatActivity() {
    var game: Game? = null
    lateinit var gameView : DrawingView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        val size = intent.extras.getInt("Size")
        val winSize = intent.extras.getInt("WinSize")

        gameView = findViewById(R.id.gameField) as DrawingView

        game = Game(size, winSize) // 3X3
        gameView.game = game
        gameView.postInvalidate() // Force refresh game View

    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        //game = savedInstanceState?.getParcelable("game")
        super.onRestoreInstanceState(savedInstanceState)

    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        //outState?.putParcelable("game", game)
        super.onSaveInstanceState(outState, outPersistentState)
    }
}
