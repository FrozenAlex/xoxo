package com.frozenalex.xoxo

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText

class MainMenu : AppCompatActivity() {
    lateinit var sizeText :EditText
    lateinit var winText :EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)
        sizeText = findViewById(R.id.sizeText) as EditText
        winText = findViewById(R.id.winText) as EditText
    }

    fun exitGame(view: View){
        view.baseline // For some reason remove all IDE warnings ^_^
        this.finish()
    }

    fun StartGame(view: View){
        val intent = Intent(view.context, GameActivity::class.java)

        intent.putExtra("Size", sizeText.text.toString().toInt())
        intent.putExtra("WinSize", winText.text.toString().toInt())
        startActivity(intent)
    }
}
