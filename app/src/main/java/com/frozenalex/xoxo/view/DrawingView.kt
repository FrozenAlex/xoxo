package com.frozenalex.xoxo.view

import android.content.Context
import android.graphics.*
import android.opengl.GLSurfaceView
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.SurfaceView
import android.view.View
import android.widget.Toast
import com.frozenalex.xoxo.model.Game


class DrawingView(context: Context, attrs: AttributeSet) : View(context,attrs) {
    val paintColor = Color.BLACK
    private var lastCoordinate: Point? = null

    // Colors
    val gridColor = Color.BLACK

    // Paints
    lateinit var drawPaint:Paint
    lateinit var gridPaint:Paint

    var circlePoints :ArrayList<PointF>
    var game: Game? = null // Link to the game object

    init {
        isFocusable = true
        isFocusableInTouchMode = true
        circlePoints = ArrayList<PointF>()
        setupPaint()
      //  getGameRect(width,height) TODO: Center game client
    }

    private fun setupPaint(){
        drawPaint = Paint()
        with(drawPaint){
            color = paintColor
            isAntiAlias = true
            strokeWidth = 10f
            style = Paint.Style.STROKE
            strokeJoin = Paint.Join.ROUND
            strokeCap = Paint.Cap.ROUND
        }

        gridPaint = Paint()
        with(gridPaint){
            color = gridColor
            isAntiAlias = true
            strokeWidth = 6f
            style = Paint.Style.FILL
            strokeJoin = Paint.Join.ROUND
            strokeCap = Paint.Cap.ROUND
        }
    }

    override fun onDraw(canvas: Canvas?) {
        if(game != null){
            drawNet(canvas, game!!.size)
            drawXO(canvas, 8f)
        }
    }

    fun drawNet(canvas: Canvas?, size:Int){
        val x = width/size
        val y = height/size
        for (i: Int in 0..size) {
            // Calculate y and x positions
            val xPos = (x*i).toFloat()
            val yPos = (y*i).toFloat()

            // Draw lines
            canvas?.drawLine(xPos,0f,xPos,height.toFloat(),gridPaint) // X
            canvas?.drawLine(0f,yPos,width.toFloat(),yPos,gridPaint) // Y
        }
    }

    fun drawXO(canvas: Canvas?, margin: Float){
        for(x: Int in 0..game!!.size-1){
            for (y:Int in 0..game!!.size-1){
                if(game!!.field[x][y] != -1) {
                    val tileWidth = (width/game!!.size/2)
                    val centerPoint = getTileCenter(x,y,game!!.size)

                    // X & O
                    if(game!!.field[x][y] == 1) drawX(centerPoint.x,centerPoint.y, tileWidth - margin, canvas)
                    if(game!!.field[x][y] == 0)canvas?.drawCircle(centerPoint.x,centerPoint.y, tileWidth - margin, drawPaint)
                }
            }
        }
    }


    /*
    * "Simplifies" drawing x by center
    */
    fun drawX(x:Float, y: Float, size: Float,canvas: Canvas?){
        canvas?.drawLine(x-size,y-size, x+size, y+size, drawPaint)  // from upper left to bottom right
        canvas?.drawLine(x+size, y-size, x-size, y+size, drawPaint)
    }


    /*
    * Gets tile center point for array element
    */
    private fun getTileCenter(x: Int,y:Int  ,size: Int) : PointF{
        val xScale = width/size
        val yScale = height/size
        return PointF(((x*xScale)+(xScale/2)).toFloat(),((y*yScale)+(yScale/2)).toFloat())
    }

    /*
    * Gets array element on tile
     */
    private fun getTileArrayIndex(x: Float,y:Float ,size: Int) : Point{
        val xScale = width/size
        val yScale = height/size
        return Point(
                Math.floor(x/xScale.toDouble()).toInt(),
                Math.floor(y/yScale.toDouble()).toInt()
        )
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event?.action == MotionEvent.ACTION_DOWN){
            val touchX = event.x
            val touchY = event.y

            if(game != null) {
                lastCoordinate = getTileArrayIndex(touchX, touchY, game!!.size)
                System.out.println("Pressed on X:" + lastCoordinate?.x + " Y:" + lastCoordinate?.y)
            }
        }

        if (event?.action == MotionEvent.ACTION_UP){
            val touchX = event.x
            val touchY = event.y

            if(game != null) {
                val point = getTileArrayIndex(touchX, touchY, game!!.size)
                System.out.println("Released on X:" + point.x + " Y:" + point.y)
                if(lastCoordinate == point){
                    if (game?.Move(point.x,point.y ) == true){
                        Toast.makeText(context, game!!.currentPlayer.toString() + " Won!", Toast.LENGTH_SHORT).show()
                        if (game != null ) game = Game(game!!.size,game!!.winSize)
                    }
                    postInvalidate()
                }
            }
        }
        return true
    }
}